# Normal
Der Content-Security-Policy-Header legt fest, von welchen Domains Javascript nachgeladen werden darf. Diese Domains sind die Website selber und 
```
https://pastebin.com 
example.com 
code.jquery.com 
https://ssl.google-analytics.com
```

# Hack:

Lade dein Javascript auf Pastebin hoch.

Beispiellösung: `https://pastebin.com/raw/E6Uwa3Wj`