# Normale Funktion

Normaler File-Upload (z.B. *jpg, *png, *gif, ...) von Dateien **kleiner 100KiB** der mit einem "succesfully uploaded!" quitiert wird.
# Hack 

Upload von shell.php (p0wny-shell https://github.com/flozz/p0wny-shell/blob/master/shell.php) 

Aufruf von /vulnerabilities/upload/../../hackable/uploads/shell.php

Man erhält eine vollwertige Shell mit www-data-Rechten.

## Dump der Database

`mysqldump -u dvwa -pSuperSecretPassword99 dvwa > Dump.sql`

# Hack mit Metasploit

Ein ausführliches Tutorial findet sich bei [hackingarticles.in](https://www.hackingarticles.in/hack-file-upload-vulnerability-dvwa-bypass-security/)
