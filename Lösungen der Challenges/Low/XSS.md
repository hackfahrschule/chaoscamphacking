# Reflected

## Normal
Eingabe des Namen (bspw. David)

## Hack
`<script>alert('You have been hacked!');</script>`

# Stored XSS

## Normal
Eingabe des Namen und eines Kommentars

## Hack
Einfügen eines Script-Tags im Kommentar bspw:
`<script>alert('You have been hacked!');</script>`

# Dom XSS

## Normal 
Auswahl einer Sprache

## Hack 
Ersetzen der Sprache in der URL mit einem Script-Snippet bspw:
`<script>alert('You have been hacked!');</script>`
