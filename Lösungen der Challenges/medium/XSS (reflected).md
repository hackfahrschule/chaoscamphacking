# XSS (Reflected)

## Normale Eingabe

Eingabe deines Namens, beispielsweise: `David`

## Exploit

Wie ein Klick auf *View Source* verrät, ersetzt die DVWA alle `<script>` Tags mit einem leeren String.


```
// Is there any input?
if( array_key_exists( "name", $_GET ) && $_GET[ 'name' ] != NULL ) {
    // Get input
    $name = str_replace( '<script>', '', $_GET[ 'name' ] );

    // Feedback for end user
    echo "<pre>Hello ${name}</pre>";
} 
```

Ein einfacher Exploit mit `<script>alert('XSS');</script>` schlägt daher fehl, da aus dieser Zeichenkette `alert('XSS');</script>` wird, was der Browser nicht als gültigen Javascript-Code interpretiert. Zum Glück gibt es aber zahlreiche Möglichkeiten, Javascript im Browser auszuführen, ohne auf die Script-Tags zurück zu greifen. Das [Open Web Application Security Project (OWASP)](https://www.owasp.org/index.php/Main_Page) hat eine [Liste](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet) zusammengestellt, die einen Großteil der verschiedenen Möglichkeiten abdeckt. 

In unserem Fall funktioniert beispielsweise ein schlichtes `David<BODY onload='alert("XSS");'>`