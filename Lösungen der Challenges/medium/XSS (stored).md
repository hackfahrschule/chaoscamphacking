# XSS (Stored)

## Normale Eingabe:

Eingabe eines Benutzernamens und eines Beitrags

## Exploit

die Schwachstelle liegt in der Eingabe des Benutzerfeldes. Zwar ist dieses in seiner Länge beschränkt, jedoch kann diese Limitierung mit den Webentwickler.innen- Werkzeugen
des Webbrowsers ohne weiteres aufgehoben werden. 

Nun muss noch der zweite Schutzmechanismus umgangen werden:
```
$name = str_replace( '<script>', '', $name ); 
```
Diese Anweisung ersetzt alle `<script>` Tags mit einem leeren String. Aus einem `<script>alert('XSS');</script>` wird entsprechend ein `alert('XSS');</script>`.
Um diese Schutzmaßnahme zu umgehen, müssen wir einen anderen Weg finden, Javascript im Browser auszuführen. Das Das [Open Web Application Security Project (OWASP)](https://www.owasp.org/index.php/Main_Page) hat eine [Liste](https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet) 
zusammengestellt, die einen Großteil der verschiedenen Möglichkeiten abdeckt. 

In unserem Beispiel funktioniert beispielsweise ein `<BODY onload=alert('XSS');>`