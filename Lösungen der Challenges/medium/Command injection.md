# Command injection

## Normale Eingabe 

Eingabe einer IP, die angepingt werden soll. Beispielsweise: `127.0.0.1`

## Exploit 

Die fortgeschrittene Schwierigkeit verbietet die Zeichen `&&` und `;`. Wir müssen also einen anderen Weg finden, die Befehle zu verketten.
Eine Möglichkeit ist, den Oder-Operator `||` zu benutzen. Dieser führt einen zweiten Befehl aus, wenn der erste fehlschlägt. Damit der Ping-Befehl abbricht,
übergeben wir ihm eine ungültige IP Adresse und hängen an diese den Oder-Operator und unseren zweiten Befehl an: `1 || echo 'hello world'`