# ChaosCampHacking

Die Dokumentation zur Hacker-Challenge auf dem Chaos Camp 2019

## Inhaltsverzeichnis
[Hier sollte unbedingt eine sinnvolle Reihenfolge der Hacks festgelegt werden]

## Benutzung bei unseren Hackfahrschulen

Die DVWA in der von uns für dich vorbereiteten virtuellen Maschine erreichst du unter der URL http://digitalcourage.dvwa/ .

Die Zugangsdaten für das Webinterface der DVWA sind:
```
Username: digitalcourage
Passswort: digitalcourage
```

## Für zuhause

Um zuhause mit einer DVWA zu arbeiten, kann man sie z.B. mittels Docker installieren. Das geht unter Debian so:
```shell
sudo apt install docker-ce
sudo docker run --rm -it -p 127.0.0.1:80:80 vulnerables/web-dvwa
```
Das war's schon! Anschließend ist die dockerisierte DVWA auf http://localhost/ erreichbar.

Dort ist das Login dann admin/password, und nach einem Click auf [Create Database] ist die DVWM bereit, gehackt zu werden.


## Lizenz
Gerne könnt ihr unsere Inhalte verwenden. Sie sind dual unter der GPL v3 und der CC BY SA 4.0 Lizenz freigegeben. 
